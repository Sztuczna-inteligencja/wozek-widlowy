## Wózek widłowy

### Usage
Python 3+ is required.
Creating a virtual environment is suggested:  
```python
    1. python3 -m venv [name]
    2. cd [name]
    3. source bin/activate (UNIX) / Scripts\activate.bat (Windows)
```
then:  
`pip3 install -r requirements.txt`  
Run the project with:  
`python3 -m src.main`