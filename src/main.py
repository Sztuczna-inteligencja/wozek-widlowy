import pygame
import os
from pygame.locals import *
from .constants import HEIGHT, WIDTH, BLOCK_SIZE, FPS, H, W, CRATE1, CRATE2, CRATE3, COLORS, LIGHT_GREY, GREY
from math import floor, ceil
from .a_star import RoutePlan, astar_search
from .terrain import Terrain, Crate, Shelf, Wall
from .decision_tree import estimator
from .neural_network import model, x_test, y_test

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, './images/forklift.png')
shelf_file = os.path.join(dirname, './images/shelf.jpg')
crate1_file = os.path.join(dirname, './images/crate1.jpg')
crate2_file = os.path.join(dirname, './images/crate2.jpg')
crate3_file = os.path.join(dirname, './images/crate3.jpg')

screen = pygame.display.set_mode((HEIGHT, WIDTH))


class Grid:
    def __init__(self):
        self.tiles = []
        ''' center initially '''
        self.x = 6
        self.y = 2
        self.dir = 'E'
        self.rotation = 0
        self.step = 0
        self.carrying = False
        self.goal = None
        self.decision = None

        self.crate1_tile_positions = [
            Crate(
                1, 10, CRATE1), Crate(
                2, 10, CRATE1), Crate(
                3, 10, CRATE1), Crate(
                4, 10, CRATE1),
            Crate(
                1, 11, CRATE1), Crate(
                2, 11, CRATE1), Crate(
                3, 11, CRATE1), Crate(
                4, 11, CRATE1), Crate(
                1, 6, CRATE1
            )
        ]

        self.crate2_tile_positions = [
            Crate(
                6, 10, CRATE2), Crate(
                7, 10, CRATE2), Crate(
                8, 10, CRATE2), Crate(
                9, 10, CRATE2),
            Crate(
                6, 11, CRATE2), Crate(
                7, 11, CRATE2), Crate(
                8, 11, CRATE2), Crate(
                9, 11, CRATE2), Crate(
                10, 3, CRATE2),
        ]

        self.crate3_tile_positions = [
            Crate(
                11, 10, CRATE3), Crate(
                12, 10, CRATE3), Crate(
                12, 11, CRATE3), Crate(
                13, 10, CRATE3),
            Crate(
                11, 11, CRATE3), Crate(
                13, 11, CRATE3), Crate(
                14, 10, CRATE3), Crate(
                14, 11, CRATE3), Crate(
                5, 3, CRATE3
            )
        ]

        self.crate_positions = self.crate1_tile_positions + \
            self.crate2_tile_positions + self.crate3_tile_positions

        self.shelves = (
            Shelf(3, 0),
            Shelf(6, 0),
            Shelf(9, 0),
            Shelf(12, 0),
            Shelf(15, 0),
            Shelf(18, 0),
        )

        self.sector_area = {'A': [(1, 0), (1, 1), (2, 0), (2, 1)], 'B': [(4, 0), (5, 0), (4, 1), (5, 1)], 'C': [(7, 0), (8, 0), (7, 1), (
            8, 1)], 'D': [(10, 0), (10, 1), (11, 0), (11, 1)], 'E': [(12, 0), (13, 1), (14, 0), (14, 1)], 'F': [(16, 0), (16, 1), (17, 0), (17, 1)]}

        self.shelf_sector_map = {
            'A': self.shelves[0],
            'B': self.shelves[1],
            'C': self.shelves[2],
            'D': self.shelves[3],
            'E': self.shelves[4],
            'F': self.shelves[5],
        }

        self.walls = set([Wall(0, 0), Wall(0, 1), Wall(3, 0), Wall(3, 1), Wall(6, 0), Wall(6, 1), Wall(9, 0), Wall(9, 1),
                          Wall(
            12, 0), Wall(
            12, 1), Wall(
            15, 0), Wall(
                15, 1), Wall(
                    18, 0), Wall(
                        18, 1),
            Wall(7, 6), Wall(8, 6), Wall(9, 6), Wall(10, 6), Wall(11, 6), Wall(12, 6), Wall(3, 4), Wall(3, 5), Wall(3, 6), Wall(3, 7), Wall(3, 8), ])

    def draw_env(self):
        shelf = pygame.image.load(shelf_file).convert_alpha()
        shelf = pygame.transform.scale(shelf, (60, 120))

        wall = pygame.image.load(shelf_file).convert_alpha()
        wall = pygame.transform.scale(wall, (60, 60))

        crate1 = pygame.image.load(crate1_file).convert_alpha()
        crate1 = pygame.transform.scale(crate1, (60, 60))

        crate2 = pygame.image.load(crate2_file).convert_alpha()
        crate2 = pygame.transform.scale(crate2, (60, 60))

        crate3 = pygame.image.load(crate3_file).convert_alpha()
        crate3 = pygame.transform.scale(crate3, (60, 60))

        crate_type_image_map = {
            CRATE1: crate1,
            CRATE2: crate2,
            CRATE3: crate3,
        }

        for index, crate_obj in enumerate(self.crate_positions):
            crate = crate_type_image_map[crate_obj.type]
            x, y = crate_obj.get_pos()
            screen.blit(crate,
                        (self.tiles[x].left,
                         self.tiles[y].left))

        for i, shelf_obj in enumerate(self.shelves):
            x, y = shelf_obj.get_pos()
            screen.blit(shelf,
                        (self.tiles[x].left,
                         self.tiles[y].left))

        for wall_obj in self.walls:
            x, y = wall_obj.get_pos()
            screen.blit(wall, (self.tiles[x].left, self.tiles[y].left))

    def draw_grid(self, h, w):
        for y in range(h):
            for x in range(w):
                rect = pygame.Rect(x * (BLOCK_SIZE + 1), y *
                                   (BLOCK_SIZE + 1), BLOCK_SIZE, BLOCK_SIZE)
                pygame.draw.rect(screen, COLORS[LIGHT_GREY], rect)
                self.tiles.append(rect)

        font = pygame.font.SysFont('Comic Sans MS', 192)

        def render_text(t): return font.render(t, False, COLORS[GREY])

        cursor = BLOCK_SIZE + 2
        for sector_name in self.shelf_sector_map.keys():
            screen.blit(render_text(sector_name), (cursor, 1))
            cursor += 3 * BLOCK_SIZE + 1

    def set_fps(self):
        clock = pygame.time.Clock()
        clock.tick(FPS)

    def set_path(self, goal):
        board_dims = (H, W)
        self.state = (self.x, self.y, self.dir)
        self.goal = goal
        self.route = RoutePlan(self.state, goal, board_dims, self.walls)
        self.search = astar_search(self.route)
        self.solution = self.search.solution()
        self.step = 0

    def inside_sector(self, sector):
        return (self.x, self.y) in self.sector_area[sector]

    def check_for_crates(self, position):
        cur_x = position[0]
        cur_y = position[1]
        dir = position[2]
        # n = (cur_x, cur_y - 1, 'N')
        # s = (cur_x, cur_y + 1, 'S')
        # e = (cur_x + 1, cur_y, 'E')
        # w = (cur_x - 1, cur_y, 'W')
        # positions = [n, s, e, w]
        crate_positions = [crate.get_pos() for crate in self.crate_positions]
        # for pos in positions:
        # pos_tuple = (pos[0], pos[1])
        pos_tuple = (cur_x, cur_y)
        if pos_tuple in crate_positions:
            return (cur_x, cur_y, self.dir)
        return None


def main():
    pygame.init()
    pygame.font.init()
    pygame.display.set_caption('AI Forklift')

    running = True

    direction = 'E'

    ''' position of the forklift, initially turned right '''

    grid = Grid()

    wall_positions = [wall.get_pos() for wall in grid.walls]

    while running:
        grid.set_fps()
        grid.draw_grid(H, W)
        grid.draw_env()
        fork_icon = pygame.image.load(filename).convert_alpha()
        fork_icon = pygame.transform.rotate(fork_icon, grid.rotation)
        fork_icon = pygame.transform.scale(fork_icon, (60, 60))
        fork_icon_x = grid.tiles[grid.x].left
        fork_icon_y = grid.tiles[grid.y].left
        screen.blit(fork_icon, (fork_icon_x, fork_icon_y))
        if grid.goal is not None:
            action = None
            if grid.step < len(grid.solution) and grid.solution is not None and len(
                    grid.solution) > 0:

                action = grid.solution[grid.step].action
                state = (grid.x, grid.y, grid.dir)
                new_x, new_y, new_dir = grid.route.result(state, action)
                ''' integracja '''
                check = grid.check_for_crates((new_x, new_y, new_dir))
                grid.dir = new_dir
                if check is not None and not grid.carrying:
                    crate = None
                    crate_index = None
                    for i, crate_obj in enumerate(grid.crate_positions):
                        x, y = crate_obj.get_pos()
                        if x == check[0] and y == check[1]:
                            crate = crate_obj
                            crate_index = i
                            break
                    if crate is not None and not grid.carrying:
                        if hasattr(crate, 'attrs'):
                            attrs = crate.attrs
                            decision = estimator.predict([attrs])[0]
                            grid.decision = decision
                            print(
                                'Decision tree, decision {}'.format(decision))
                        else:
                            decision = model.predict(
                                x_test[crate.image].reshape(1, 28, 28, 1))
                            decision = decision.argmax()
                            print(
                                'Image recognition, decision {}'.format(decision))
                            shelf_map = {
                                1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F'}
                            grid.decision = shelf_map[decision]
                        goal_shelf = grid.shelf_sector_map[grid.decision]
                        grid.carrying = True
                        shelf_x, shelf_y = goal_shelf.get_pos()
                        grid.set_path((shelf_x - 1, shelf_y + 1, 'N'))
                        print(f'Sector {decision}')
                ''' --- '''
                action = grid.solution[grid.step].action
                new_x, new_y, new_dir = grid.route.result(state, action)
                grid.x = new_x
                grid.y = new_y
                grid.dir = new_dir

                if grid.carrying:
                    grid.crate_positions[crate_index] = Crate(
                        grid.x, grid.y, crate.type)
                    if grid.inside_sector(grid.decision):
                        if crate_index is not None:
                            grid.crate_positions.pop(crate_index)
                            crate_index = None
                        grid.carrying = False

            grid.step += 1

            if action == 'rotate_right':
                grid.rotation -= 90
            if action == 'rotate_left':
                grid.rotation += 90

            if grid.route.goal_test(state):
                grid.carrying = False

        for event in pygame.event.get():
            key = pygame.key.get_pressed()
            if key[pygame.K_UP]:
                grid.y -= 1
            if key[pygame.K_DOWN]:
                grid.y += 1
            if key[pygame.K_LEFT]:
                grid.x -= 1
            if key[pygame.K_RIGHT]:
                grid.x += 1

            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                x_block = floor(x / BLOCK_SIZE)
                y_block = floor(y / BLOCK_SIZE)

                if (x_block, y_block) not in wall_positions:
                    grid.set_path((x_block, y_block, 'N'))
        pygame.display.flip()


if __name__ == '__main__':
    main()
