from numpy.random import randint
from numpy import array
'''
Drzewo decyzyjne odpowiada za wybór, do jakiego sektora powinna trafić paczka po podniesieniu przez wózek.

Atrybuty paczki i możliwe zakresy ich wartości to:
    • waga (0-n)
    • szerokość (10-200)
    • długość (10-200)
    • kruchość (1-10)
    • priorytet (1-10)
    • materiał (metal | drewno | plastik | porcelana | szkło)

Paczka może trafić do jednego z pięciu sektorów: A, B, C, D, E, F.

Do sektora F bezwzględnie trafiają paczki z wysokim priorytetem (większym niż 6), ma on pierwszeństwo nad innymi sektorami niezależnie od innych atrybutów. Sektor E jest drugi w hierarchii, jeśli paczka ma dostatecznie wysoki stopień kruchości (większ niż 7) lub jest ze szkła lub porcelany to trafia do niego. Na trzecim miejscu stoi sektor D,  do którego trafiają ciężkie paczki (waga powyżej 40).
Jeśli paczka nie jest krucha, priorytetowa lub ciężka to trafia do jednego z sektorów A, B, C. Każdy z nich jest przeznaczony na odpowiednie rozmiary paczek, odpowiednio o długości i szerokości:
    • A	[10-n]
    • B	(n-1n]
    • C	(1n-200]

'''


attributes = ['weight', 'width', 'length', 'fragility', 'urgency', 'material']

materials = {
    0: 'metal',
    1: 'wood',
    2: 'plastic',
    3: 'glass',
    4: 'porcelain',
}


def generate_test_cases(n):
    test_data = []
    decisions = []

    # sector A - small boxes
    for i in range(n):
        material_index = randint(low=0, high=5)
        if material_index > 2: # if the package is made of glass or porcelain, then it goes to the fragile sector
            decisions.append('E')
        else:
            decisions.append('A') # otherwise, the randomized data fits sector A criteria
        test_data.append([
            randint(low=0, high=41),
            randint(low=10, high=51),
            randint(low=10, high=51),
            randint(low=1, high=8),
            randint(low=1, high=7),
            material_index,
        ])


    ''' data[0] < 40, data[1], data[2] = [10, n], data[3] = [1, 7], data[4] = [1, 6], data[5] = [0,2] (indeksy)'''
    # sector B - medium boxes
    for i in range(n):
        material_index = randint(low=0, high=5)
        if material_index > 2:
            decisions.append('E')
        else:
            decisions.append('B')
        test_data.append([
            randint(low=0, high=41),
            randint(low=51, high=126),
            randint(low=51, high=126),
            randint(low=1, high=8),
            randint(low=1, high=7),
            material_index,
        ])

    # sector C - large boxes
    for i in range(n):
        material_index = randint(low=0, high=5)
        if material_index > 2:
            decisions.append('E')
        else:
            decisions.append('C')
        test_data.append([
            randint(low=0, high=41),
            randint(low=126, high=201),
            randint(low=126, high=201),
            randint(low=1, high=8),
            randint(low=1, high=7),
            material_index,
        ])

    # sector D - heavy boxes
    for i in range(n):
        material_index = randint(low=0, high=5)
        if material_index > 2:
            decisions.append('E')
        else:
            decisions.append('D')
        test_data.append([
            randint(low=41, high=51),
            randint(low=1, high=201),
            randint(low=1, high=201),
            randint(low=1, high=8),
            randint(low=1, high=7),
            material_index,
        ])

    # sector E - fragile boxes
    for i in range(n):
        decisions.append('E')
        test_data.append([
            randint(low=1, high=51),
            randint(low=1, high=201),
            randint(low=1, high=201),
            randint(low=8, high=11),
            randint(low=1, high=7),
            randint(low=0, high=5),
        ])

    # sector F - urgent boxes
    for i in range(n):
        decisions.append('F')
        test_data.append([
            randint(low=1, high=51),
            randint(low=1, high=201),
            randint(low=1, high=201),
            randint(low=1, high=11),
            randint(low=7, high=11),
            randint(low=0, high=5),
        ])

    return (array(test_data), array(decisions))


test_cases, decisions = generate_test_cases(3000)

with open('test_data.txt', 'w') as f:
    f.write('Test cases\n')
    for case in test_cases:
        f.write(str(case) + '\n')

    f.write('Decisions\n')
    for d in decisions:
        f.write(str(d) + '\n')

