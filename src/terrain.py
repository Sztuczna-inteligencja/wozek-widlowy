from .constants import CRATE_TYPES
import random


class Terrain:
    def __init__(self, x, y):
        self.pos = (x, y)

    def get_pos(self):
        return self.pos


class Crate(Terrain):
    def __init__(self, x, y, _type):
        if _type not in CRATE_TYPES:
            raise ValueError('Invalid crate type')
        super().__init__(x, y)
        self.type = _type

        set_attrs = random.randint(1, 2)
        if set_attrs == 1:
            self.attrs = self.randomize_attrs()
        else:
            self.image = random.randint(9984, 9989)

    def randomize_attrs(self):
        r = random.randint(0, 4)
        sectors = ('A', 'B', 'C', 'D', 'E', 'F')
        example_attrs = {
            'A': [20, 20, 40, 4, 5, 2],
            'B': [20, 70, 70, 5, 5, 2],
            'C': [20, 140, 140, 7, 2, 1],
            'D': [45, 120, 120, 9, 6, 3],
            'E': [20, 120, 120, 9, 6, 3],
            'F': [20, 120, 130, 7, 8, 4],
        }
        return example_attrs[sectors[r]]


class Shelf(Terrain):
    pass


class Wall(Terrain):
    pass
