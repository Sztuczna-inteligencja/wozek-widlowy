''' screen properties '''
BLOCK_SIZE = 60
# amount of tiles height and width-wise
H = 13
W = 19
WIDTH = H * (BLOCK_SIZE + 1)
HEIGHT = W * (BLOCK_SIZE + 1)

''' colors '''
WHITE = 1
BLACK = 2
LIGHT_GREY = 3
GREY = 4
COLORS = {
    LIGHT_GREY: (220, 220, 220),
    GREY: (192, 192, 192),
    BLACK: (0, 0, 0),
    WHITE: (255, 255, 255),
}

''' general '''
FPS = 2

''' crates '''
CRATE1 = 1
CRATE2 = 2
CRATE3 = 3
CRATE_TYPES = (CRATE1, CRATE2, CRATE3)
