''' abstract class based on the formal problem definition from the book '''


class Problem:
    '''
    @initial_state: Initial state that the agent starts in
    @goal: single goal or a set of goal states
    '''

    def __init__(self, initial_state, goal=None):
        self.initial_state = initial_state
        self.goal = goal

    '''
    A description of the possible actions available to the agent.
    Given a particular state s returns the set of actions that can be executed in s.
    '''

    def actions(self, state):
        raise NotImplementedError

    '''determines whether a given state is a goal state'''

    def goal_test(self, state):
        raise NotImplementedError

    ''' assigns a numeric cost to each path '''

    def path_cost(self, cost_up_to, state_from, state_to, action):
        raise NotImplementedError

    def step_cost(state_from, action, state_to):
        raise NotImplementedError

    ''' returns the state that results from doing action a in state s '''

    def result(self, s, a):
        raise NotImplementedError

    ''' returns the cost of an action from a state '''

    def step_cost(self, state, action):
        raise NotImplementedError
