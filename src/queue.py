import heapq


class PriorityQueue:
    def __init__(self, func=lambda x: x):
        self.heap = []
        self.func = func

    def append(self, item):
        heapq.heappush(self.heap, (self.func(item), item))

    def pop(self):
        try:
            # [1] because the data format is (weight, node)
            return heapq.heappop(self.heap)[1]
        except IndexError:
            raise Exception('Tried to pop from an empty PriorityQueue')

    def remove(self, item):  # first occurrence
        self.heap.remove(item)
        heapq.heapify(self.heap)

    def get(self, key):
        for _, item in self.heap:
            if item == key:
                return item
        return None

    def __contains__(self, item):  # infix in operator
        return (self.func(item), item) in self.heap
