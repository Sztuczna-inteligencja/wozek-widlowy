'''
n.STATE : the state in the state space to which the node corresponds;
n.PARENT : the node in the search tree that generated this node;
n.ACTION : the action that was applied to the parent to generate the node;
n.PATH-COST : the cost, traditionally denoted by g(n), of the path from the initial state
to the node, as indicated by the parent pointers.
'''


class Node:
    def __init__(self, state, parent=None, action=None, path_cost=0):
        self.state = state
        self.parent = parent
        self.action = action
        self.path_cost = path_cost

    def child_node(self, problem, action):
        next_state = problem.result(self.state, action)
        return Node(
            state=next_state,
            parent=self,
            action=action,
            path_cost=self.path_cost +
            problem.step_cost(
                self.state,
                action,
                next_state))

    def solution(self):
        '''
        we use the solution function to return the sequence
        of actions obtained by following parent pointers back to the root
        '''
        node = self
        solution = []
        while node:
            solution.append(node)
            node = node.parent
        return list(reversed(solution))

    def __lt__(self, cmp):
        return self.path_cost <= cmp.path_cost

    def __repr__(self):
        return "Node {}".format(self.state)
