from id3 import Id3Estimator, export_graphviz
from .test_data import attributes, test_cases, decisions

id3 = Id3Estimator()

estimator = Id3Estimator()
estimator.fit(test_cases, decisions)

export_graphviz(estimator.tree_, 'tree.dot', attributes)
